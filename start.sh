#!/usr/bin/env bash
docker-compose up -d --scale firefox=2 --scale chrome=2
docker-compose run uitests  mvn clean verify -Dremote=true -DseleniumGridURL=http://hub:4444/wd/hub -Dbrowser=chrome -Dthreads=1

