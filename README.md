# Selenium-test-project
### Prerequisites:
1. Installed Apache Maven
2. Installed Docker (optional) if you want to run your tests against dockerised Selenium Grid)

To run a test you have to:
1. Clone this project to your machine
2. Go inside of cloned folder
3. Execute `./docker-grid.sh` to run your test against dockerised Selenium Grid or `mvn clean verify` to run your tests locally on Google Chrome browser in headless mode.
All needed dependencies (including Selenium webdrivers) will be automatically downloaded and your test(s) will be executed.

### Features:

You can specify which browser to use for your test(s) by these parameters:

- -Dbrowser=firefox
- -Dbrowser=chrome
- -Dbrowser=ie
- -Dbrowser=edge
- -Dbrowser=opera

For example - `mvn clean verify -Dbrowser=chrome` will run your test(s) in headless mode on Google Chrome

If you want to toggle the use of Chrome or Firefox in headless mode set the headless flag (by default the headless flag is set to true)

- -Dheadless=true
- -Dheadless=false

For example - `mvn clean verify -Dbrowser=firefox -Dheadless=false` will run your test(s) in normal mode on Firefox

You don't need to worry about downloading the IEDriverServer, EdgeDriver, ChromeDriver , OperaChromiumDriver, or GeckoDriver binaries, this project will do that for you automatically.

You can even specify multiple threads (you can do it on a grid as well!):

- -Dthreads=2

You can also specify a proxy to use

- -DproxyEnabled=true
- -DproxyHost=localhost
- -DproxyPort=8080

If the tests fail screenshots will be saved in ${project.basedir}/target/screenshots

If you need to force a binary-files for webdrivers overwrite you can do:

- -Doverwrite.binaries=true

### Docker features:
This project is completely "Docker-ready" and your test(s) could be easily executed on dockerised Selenium Grid. Just run:

`./docker-grid.sh`

and in few moments will be launched Selenium Grid Hub + Firefox and Chrome nodes Docker containers which are already configured and your test(s) will be executed. 

### Useful information
        
- The maven-failsafe-plugin will pick up any files that end in 'IT' by default.  You can customise this is you would prefer to use a custom identifier for your Selenium tests.
