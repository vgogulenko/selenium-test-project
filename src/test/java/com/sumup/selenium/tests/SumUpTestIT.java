package com.sumup.selenium.tests;

import com.sumup.selenium.config.DriverBase;
import com.sumup.selenium.config.Locators;
import com.sumup.selenium.page_objects.SumUpHomePage;
import com.sumup.selenium.page_objects.SumUpLoginPage;
import com.sumup.selenium.page_objects.SumUpTransactionsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class SumUpTestIT extends DriverBase {

    private ExpectedCondition<Boolean> pageTitleStartsWith(final String searchString) {
        return driver -> {
            assert driver != null;
            return driver.getTitle().toLowerCase().startsWith(searchString.toLowerCase());
        };
    }

    private ExpectedCondition<Boolean> textIspPresentOnPage(final String text) {
        return driver -> {
            assert driver != null;
            return driver.getPageSource().contains(text);
        };
    }

    @Test
    public void openTransactionPage() throws Exception {

        String page_loading_complete = "return document.readyState==\"complete\";";
        WebDriver driver = getDriver();
        WebDriverWait wait = new WebDriverWait(driver,20);

        // Open SumUp customers Home page
        System.out.println("Open https://sumup.com/...");
        driver.get("https://sumup.com/");

        SumUpHomePage sumUpHomePage = new SumUpHomePage();


        // Switch to Modal window which is appearing if you are in different country than US if it was appeared
        try{
            driver.switchTo().activeElement();
            sumUpHomePage.clickOnproceedToUSBtn();
        } catch(Exception e){
            System.out.println("No modal frame was found. Going further...");
        }

        // Click on Login link on Home page
        System.out.println("Click on Login link on Home page...");
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(Locators.
                getValue("homePage.loginLink"))));
        sumUpHomePage.clickOnLoginLink();

        // Wait when second tab will be opened
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));

        // Switch to new opened tab with Login page
        ArrayList<String> tabs2 = new ArrayList<>(driver.getWindowHandles());
        int numberOfTabs = tabs2.size();
        driver.switchTo().window(tabs2.get(1));
        wait.until(ExpectedConditions.jsReturnsValue(page_loading_complete));

        SumUpLoginPage sumUPLoginPage = new SumUpLoginPage();

        // Wait when elements on Login page will be loaded
        wait.until(ExpectedConditions.jsReturnsValue(page_loading_complete));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locators.
                getValue("loginPage.loginBtn.xpath"))));

        // Wait for the Login page to load, timeout after 20 seconds
        wait.until(pageTitleStartsWith("SumUP Dashboard"));
        wait.until(ExpectedConditions.jsReturnsValue(page_loading_complete));

        // Type in username, password, choose "English" as a language and click on "Login" button
        System.out.println("Type in username, password, choose \"English\" as " +
                "a language and click on \"Login\" button...");
        sumUPLoginPage.enterUsername("viktor.gogulenko@gmail.com");
        sumUPLoginPage.enterPassword("TestSoftware2018");
        sumUPLoginPage.selectLanguage("English");
        sumUPLoginPage.clickOnLoginBtn();

        // Wait for the Dashboard (Overview) page to load
        wait.until(ExpectedConditions.jsReturnsValue(page_loading_complete));
        wait.until(pageTitleStartsWith("SumUP Dashboard"));
        wait.until(textIspPresentOnPage("Overview"));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locators.
                getValue("transactionsPage.transactionsMenuItem.xpath"))));

        SumUpTransactionsPage sumUpTransactionsPage = new SumUpTransactionsPage();


        // Click on Transactions menu item to open page with user's transactions
        System.out.println("Click on Transactions menu item to open page with user's transactions...");
        wait.until(ExpectedConditions.jsReturnsValue(page_loading_complete));
        sumUpTransactionsPage.clickOnTransactionsMenuItem();

        // Select all filters to see any transactions for the six months period
        System.out.println("Select all filters to see any transactions for the six months period...");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locators.
                getValue("transactionsPage.paymentTypeBtn.xpath"))));
        sumUpTransactionsPage.clickOnPyamentTypeBtn();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locators.
                getValue("transactionsPage.cardFilterChkbox.xpath"))));
        sumUpTransactionsPage.clickOnCardFilter();
        sumUpTransactionsPage.clickOnCashFilter();
        sumUpTransactionsPage.clickOnApplyBtn();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locators.
                getValue("transactionsPage.statusBtn.xpath"))));
        sumUpTransactionsPage.clickOnStatusBtn();
        sumUpTransactionsPage.clickOnSuccessfulTypeFilter();
        sumUpTransactionsPage.clickOnFailedTypeFilter();
        sumUpTransactionsPage.clickOnRefundTypeFilter();
        sumUpTransactionsPage.clickOnChargeBackTypeFilter();
        sumUpTransactionsPage.clickOnApplyBtn();
        wait.until(ExpectedConditions.jsReturnsValue(page_loading_complete));
        wait.until(textIspPresentOnPage("We couldn’t find anything that matches your search."));

        // Logout from application
        System.out.println("Logout from application...");
        sumUpTransactionsPage.clickOnAvatar();
        sumUpTransactionsPage.clickOnLogoutBtn();
        // Verify that we successfully made a logout
        wait.until(ExpectedConditions.jsReturnsValue(page_loading_complete));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locators.
                getValue("loginPage.loginBtn.xpath"))));
    }
}