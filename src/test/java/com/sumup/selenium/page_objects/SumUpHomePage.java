package com.sumup.selenium.page_objects;

import com.lazerycode.selenium.util.Query;
import com.sumup.selenium.config.DriverBase;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.sumup.selenium.config.Locators;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SumUpHomePage {

    private final RemoteWebDriver driver = DriverBase.getDriver();
    private WebDriverWait wait = new WebDriverWait(driver, 20);
    private Query loginLink = new Query(By.xpath(Locators.getValue("homePage.loginLink")), driver);
    private Query proceedToUSbtn = new Query(By.xpath(Locators.getValue("homePage.proceedToUSbtn.xpath")), driver);


    public SumUpHomePage() throws Exception {

    }

    // Click on 'Proceed to the U.S. website' if modal window appeared
    public SumUpHomePage clickOnproceedToUSBtn() throws InterruptedException {
        proceedToUSbtn.findWebElement().click();
        Thread.sleep(1000);
        return this;
    }

    public SumUpHomePage clickOnLoginLink() {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Locators.getValue(
                "homePage.loginLink"))));
        loginLink.findWebElement().click();
        return this;
    }



}