package com.sumup.selenium.page_objects;

import com.sumup.selenium.config.DriverBase;
import com.lazerycode.selenium.util.Query;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.sumup.selenium.config.Locators;

public class SumUpTransactionsPage {

    private final RemoteWebDriver driver = DriverBase.getDriver();

    private Query transactionsMenuItem = new Query(By.xpath(Locators.getValue(
            "transactionsPage.transactionsMenuItem.xpath")), driver);
    private Query paymentTypeBtn = new Query(By.xpath(Locators.getValue(
            "transactionsPage.paymentTypeBtn.xpath")), driver);
    private Query cardFilterChkbox = new Query(By.xpath(Locators.getValue(
            "transactionsPage.cardFilterChkbox.xpath")), driver);
    private Query cashFilterChkbox = new Query(By.xpath(Locators.getValue(
            "transactionsPage.cashFilterChkbox.xpath")), driver);
    private Query applyBtn = new Query(By.xpath(Locators.getValue(
            "transactionsPage.applyBtn.xpath")), driver);
    private Query statusBtn = new Query(By.xpath(Locators.getValue(
            "transactionsPage.statusBtn.xpath")), driver);
    private Query successfulTypeFilterChkbox = new Query(By.xpath(Locators.getValue(
            "transactionsPage.successfulTypeFilterChkbox.xpath")), driver);
    private Query failedTypeFilterChkbox = new Query(By.xpath(Locators.getValue(
            "transactionsPage.failedTypeFilterChkbox.xpath")), driver);
    private Query refundTypeFilterChkbox = new Query(By.xpath(Locators.getValue(
            "transactionsPage.refundTypeFilterChkbox.xpath")), driver);
    private Query chargeBackTypeFilterChkbox = new Query(By.xpath(Locators.getValue(
            "transactionsPage.chargeBackTypeFilterChkbox.xpath")), driver);
    private Query userAvatar = new Query(By.xpath(Locators.getValue("logout.userAvatar.xpath")), driver);
    private Query logoutBtn = new Query(By.xpath(Locators.getValue("logout.logoutBtn.xpath")), driver);


    public SumUpTransactionsPage() throws Exception {

    }

    public SumUpTransactionsPage clickOnTransactionsMenuItem() {
        transactionsMenuItem.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnPyamentTypeBtn(){
        paymentTypeBtn.findWebElement().click();
        return this;
    }
    public SumUpTransactionsPage clickOnCardFilter(){
        cardFilterChkbox.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnCashFilter(){
        cashFilterChkbox.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnStatusBtn(){
        statusBtn.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnSuccessfulTypeFilter(){
        successfulTypeFilterChkbox.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnFailedTypeFilter(){
        failedTypeFilterChkbox.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnRefundTypeFilter(){
        refundTypeFilterChkbox.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnChargeBackTypeFilter(){
        chargeBackTypeFilterChkbox.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnApplyBtn(){
        applyBtn.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnAvatar(){
        userAvatar.findWebElement().click();
        return this;
    }

    public SumUpTransactionsPage clickOnLogoutBtn(){
        logoutBtn.findWebElement().click();
        return this;
    }
}