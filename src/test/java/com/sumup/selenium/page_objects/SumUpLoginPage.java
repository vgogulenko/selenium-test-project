package com.sumup.selenium.page_objects;

import com.sumup.selenium.config.DriverBase;
import com.lazerycode.selenium.util.Query;
import com.sumup.selenium.config.Locators;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SumUpLoginPage {

    private final RemoteWebDriver driver = DriverBase.getDriver();

    private Query usernameField = new Query(By.id(Locators.getValue("loginPage.usernameField.id")), driver);
    private Query passwordField = new Query(By.id(Locators.getValue("loginPage.passwordField.id")), driver);
    private Query loginBtn = new Query(By.xpath(Locators.getValue("loginPage.loginBtn.xpath")), driver);
    private Query languageDropDown = new Query(By.xpath(Locators.getValue("loginPage.languageDropDown.xpath")), driver);

    public SumUpLoginPage() throws Exception {
    }

    public SumUpLoginPage enterUsername(String username) {
        usernameField.findWebElement().clear();
        usernameField.findWebElement().sendKeys(username);
        return this;
    }

    public SumUpLoginPage enterPassword(String password) {
        passwordField.findWebElement().clear();
        passwordField.findWebElement().sendKeys(password);
        return this;
    }

    public SumUpLoginPage clickOnLoginBtn() {
        loginBtn.findWebElement().click();
        return this;
    }

    public SumUpLoginPage selectLanguage(String language){
        languageDropDown.findSelectElement().selectByVisibleText(language);
        return this;
    }

}