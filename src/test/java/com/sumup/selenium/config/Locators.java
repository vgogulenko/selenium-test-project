package com.sumup.selenium.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;


public class Locators {

    private static Config locatorsConfig = ConfigFactory.load("locators");

    private Locators() {
    }

    public static String getValue(String key) {
        return locatorsConfig.getString(key);
    }

}
